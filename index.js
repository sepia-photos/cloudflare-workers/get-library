addEventListener('fetch', event => {
    event.respondWith(handleRequest(event.request))
})

/**
 * Respond to the request
 * @param {Request} request
 */
async function handleRequest(request) {
    // split access token into the right parts with the right encoding
    const accessToken = getCookie(request, "accessToken");
    const partialToken = accessToken.split('.').slice(0, 2).join('.');
    let signaturePart = accessToken.split('.')[2];

    const username = JSON.parse(atob(accessToken.split('.')[1])).sub;

    signaturePart = signaturePart.replace(/-/g, "+");
    signaturePart = signaturePart.replace(/_/g, "/");

    // throw error if the access token is invalid
    if (!await verify(str2ab(partialToken), str2ab(atob(signaturePart))))
        return new Response("", {status: 401})

    // API_URL is set as an env variable
    // fetch list of files in user's library from mediaRecords API
    const response = await fetch(API_URL+"?username="+encodeURIComponent(username)).then(res => {
        return res;
    });

    response = new Response(response.body, response)
    response.headers.set("Access-Control-Allow-Origin", "*")

    return response;
}

/**
 * Everything below this is used to verify the access token
 * it uses the builtin Crypto API to verify a JWT
 */

function getCookie(request, name) {
  let result = ""
  const cookieString = request.headers.get("Cookie")
  if (cookieString) {
    const cookies = cookieString.split(";")
    cookies.forEach(cookie => {
      const cookiePair = cookie.split("=", 2)
      const cookieName = cookiePair[0].trim()
      if (cookieName === name) {
        const cookieVal = cookiePair[1]
        result = cookieVal
      }
    })
  }
  return result
}

async function verify (encoded, signature) {
    const publicKey = await AUTH.get("publicKey");

    const keyObject = await importPublicKey(publicKey);

    return await crypto.subtle.verify(
        {
            name: "ECDSA",
            hash: {name: "SHA-256"},
        },
        keyObject,
        signature,
        encoded
    ).catch((error) => {
        console.log(error);
    });
}

function str2ab(str) {
    const buf = new ArrayBuffer(str.length);
    const bufView = new Uint8Array(buf);
    for (let i = 0, strLen = str.length; i < strLen; i++) {
        bufView[i] = str.charCodeAt(i);
    }
    return buf;
}

async function importPublicKey(pem) {
    // fetch the part of the PEM string between header and footer
    const pemHeader = "-----BEGIN PUBLIC KEY-----";
    const pemFooter = "-----END PUBLIC KEY-----";
    const pemContents = pem.substring(pemHeader.length, pem.length - pemFooter.length);
    // base64 decode the string to get the binary data
    const binaryDerString = atob(pemContents);
    // convert from a binary string to an ArrayBuffer
    const binaryDer = str2ab(binaryDerString);

    return await crypto.subtle.importKey(
        "spki",
        binaryDer,
        {
            name: "ECDSA",
            hash: {name: "SHA-256"},
            namedCurve: "P-256"
        },
        false,
        ["verify"]
    );
}
