# get-library

Sepia's backend consists of serverless functions, MongoDB, and object storage. The object storage is hosted on Backblaze B2, and all requests to the API are handled by Cloudflare workers. When necessary, information is passed on to IBM cloud functions which can interface with the database.

This worker handles requests with the /getLibrary endpoint and passes on information to the "listFiles" IBM cloud function. It's used in the initial sync after a user downloads the app and returns a list in JSON format of every photo/video in their library in chronological order with UUIDs and other information.

## Deployment

Right now there is no deployment pipeline configuration setup. To deploy this yourself you either create a Cloudflare Worker and manually paste in the contents of index.js or configure [wrangler](https://developers.cloudflare.com/workers/cli-wrangler/install-update), Cloudflare's cli tool for workers.
